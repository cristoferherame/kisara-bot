module.exports = (feedback_id) => (ctx) => {
  const message = ctx.message.text.replace(/^[^ ]+/, "");
  if (message) {
    ctx.forwardMessage(feedback_id);
    return "• ¡Gracias por tu comentario!";
  } else {
    return "• Para enviar un comentario escribe /feedback seguido de tu comentario.";
  }
};
