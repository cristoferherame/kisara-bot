module.exports = (axios, url) => (ctx) => {
  const message = ctx.message.text.replace(/^[^ ]+/, "");
  if (message) {
    return axios
      .get(
        `${url}?action=opensearch&format=json&search=` +
          `${encodeURIComponent(message)}`
      )
      .then((res) => {
        const names = res.data[1];
        const urls = res.data[3];
        if (names.length == 0) return "• Resultado(/s) no encontrados.";
        return (
          "● Resultados:\n\n" +
          names
            .map((val, index) => `<a href="${urls[index]}">${val}</a>`)
            .join("\n")
        );
      });
  } else return Promise.resolve("• Falta la consulta de para la búsqueda.");
};
