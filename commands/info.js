module.exports = () => (ctx) => {
  let text = "";
  const msg = ctx.message;
  text += `● ID del Mensaje: \`${msg.message_id}\`\n`;
  text += `• ID del Chat: \`${msg.chat.id}\`\n`;
  text += `• ID del Usuario: \`${msg.from.id}\`\n`;
  if (msg.reply_to_message) {
    const reply = msg.reply_to_message;
    text += "\n*Responde a*\n";
    text += `● ID del Mensaje: \`${reply.message_id}\`\n`;
    text += `• ID del Chat: \`${reply.chat.id}\`\n`;
    text += `• ID del Usuario: \`${reply.from.id}\`\n`;

    if (reply.forward_from || reply.forward_from_chat) {
      const forward = reply.forward_from
        ? reply.forward_from
        : reply.forward_from_chat;
      text += "\n*Reenviar desde*\n";
      if (reply.forward_from) text += "● ID del Usuario: ";
      else text += "● ID del Canal: ";
      text += `\`${forward.id}\`\n`;
      text += "• Fecha del Mensaje: `";
      const date = new Date(reply.forward_date * 1000);
      text += date.toUTCString();
      text += "`";
    }
  }
  return text;
};
