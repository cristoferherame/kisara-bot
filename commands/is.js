module.exports = (random) => (ctx) => {
  const text_array = ctx.message.text.split(" ");
  if (text_array.length == 1)
    return "• Ya sabes, también tienes que hacer una pregunta.";

  return random(
    [
      [
        "Si",
        "Sip",
        "Claro",
        "Así es",
        "Si, si",
        "Si...",
        "Si, claro",
        "Tal vez",
        "Quizá",
      ],
      ["Quizás", "No", "Nop", "Nah", "Meh", "Ño", "Ñao", "Para nada", "nunca"],
    ][Math.round(Math.random())]
  )();
};
