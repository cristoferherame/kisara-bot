const random = require("./random");
const insults_fun = require("./insult");
const words_fun = require("./words");
const is = require("./is");
const weebify = require("./weebify");
const absurdify = require("./absurdify");
const feedback = require("./feedback");
const media_wiki = require("./media_wiki");
const info = require("./info");
const expand = require("./expand");
const roleplay = require("./roleplay");
const suggest = require("./suggest");

module.exports = (
  bot,
  [questions, kys, insults, commands_list, words, roleplay_data],
  feedback_id,
  apiToken,
  ugokiRoot,
  axios
) => {
  bot.command("question", (ctx) => ctx.reply(random(questions)()));
  bot.command("word", (ctx) => ctx.reply(random(words)()));
  bot.command("words", (ctx) => ctx.reply(words_fun(random, words)(ctx)));
  bot.telegram.getMe().then((bot_user) => {
    const default_text = (command, text) =>
      `• ¿Tú quieres hacerte ${text} ` +
      `a ti mismo?\nSi no, responde con /${command} para usarlo en otro.` +
      `  o puedes enviar /${command} <@usuario>.\n• Si quieres ` +
      `más mensajes de /${command} envía un comentario con /feedback`;

    bot.command("insult", (ctx) =>
      ctx.reply(
        insults_fun(
          random,
          insults,
          default_text("insult", "insult"),
          "• Mira con quien hablas " + " tú.",
          ["@" + bot_user.username, bot_user.firstName]
        )(ctx)
      )
    );

    bot.command("kys", (ctx) =>
      ctx.reply(
        insults_fun(
          random,
          kys,
          default_text("kys", "kill"),
          "• ¿Un mediocre como tú cree que puede asesinarme? sigue soñando.",
          ["@" + bot_user.username, bot_user.firstName]
        )(ctx)
      )
    );
  });

  bot.command("commands", (ctx) =>
    ctx.reply(commands_list.join("\n"), { parse_mode: "html" })
  );
  bot.command("is", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("are", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("can", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("will", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("shall", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("was", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("do", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("does", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("did", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("should", (ctx) => ctx.reply(is(random)(ctx)));
  bot.command("coin", (ctx) => ctx.reply(random(["💶", "💵"])()));
  bot.command("help", (ctx) =>
    ctx.reply(
      "• Puedes comprobar /commands " +
        "para obtener una breve descripción general o consulte la [Página de ayuda]" +
        "(https://t.me/soly7590/833).",
      { parse_mode: "Markdown" }
    )
  );
  bot.command("rate", (ctx) =>
    ctx.reply(
      "[¡Vota por mi en el Directorio de " +
        "Telegram!](https://t.me/tgdrbot?start=kisara_bot)",
      { parse_mode: "Markdown" }
    )
  );
  bot.command("weebify", (ctx) => ctx.reply(weebify()(ctx)));
  bot.command("absurdify", (ctx) => ctx.reply(absurdify()(ctx)));
  bot.command("feedback", (ctx) => ctx.reply(feedback(feedback_id)(ctx)));
  bot.command("wiki", (ctx) =>
    media_wiki(
      axios,
      "https://es.wikipedia.org/w/api.php"
    )(ctx).then((x) => ctx.reply(x, { parse_mode: "HTML" }))
  );
  bot.command("arch_wiki", (ctx) =>
    media_wiki(
      axios,
      "https://wiki.archlinux.org/api.php"
    )(ctx).then((x) => ctx.reply(x, { parse_mode: "HTML" }))
  );
  bot.command("info", (ctx) =>
    ctx.reply(info()(ctx), { parse_mode: "Markdown" })
  );
  bot.command("expand", (ctx) => ctx.reply(expand(words)(ctx)));
  bot.command("start", (ctx) =>
    ctx.reply(
      "● Buenas, Yo soy kisara fui bautizada por @bega_9494.\n• Sí tu me estás " +
        "escribiendo en privado probablemente lo estés haciendo mal " +
        "añademe a tú grupo para más diversión.\n• También puedes enviarme comentarios, usando /feedback"
    )
  );
  bot.command("donate", (ctx) =>
    ctx.reply(
      "• Muchas gracias por considerar donar." +
        "\n• Las donaciones se usarán para mantener a Quadnite Bot, su desarrollo y la infraestructura, tú puedes " +
        "donar desde aquí: https://liberapay.com/ceda_ei/"
    )
  );

  function getGetGif(command) {
    const alias = roleplay_data[command].alias;
    if (alias) return getGetGif(alias);

    return () =>
      axios.get(`category/${command}/gif`, {
        baseURL: ugokiRoot,
      });
  }

  function getForms(name) {
    if (roleplay_data[name].forms) return roleplay_data[name].forms;
    return getForms(roleplay_data[name].alias);
  }

  // Add all roleplay commands
  Object.keys(roleplay_data).map((command) =>
    bot.command(command, (ctx) =>
      roleplay(getForms(command), getGetGif(command))(ctx)
    )
  );

  bot.command("suggest", (ctx) => suggest(axios, apiToken, ugokiRoot)(ctx));
};
