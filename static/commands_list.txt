<b>● Comandos de extras:</b>

• /coin - Lanza una moneda virtual.
• /wiki - Busca información en la Wiki.
• /arch_wiki - Buscar en la wiki de Arch.
• /kys - ¿Con ganas de matar a alguien?.
• /insult - Como era de esperarse, son insultos.

<b>● Comandos de juegos de palabras:</b>

• /question - Obtienes una pregunta al azar.
• /word - Obtienes una palabra al azar.
• /words - Obtienes varias palabras al azar.
• /weebify - Weebifica el texto dado.
• /absurdify - hAs tU tExtO aBsUrDo.
• /expand - Expande una abreviatura dada.

<b>● Comandos de reacción:</b>
<code>
• angry       • bite
• blush       • bored
• bonk        • boop
• chase       • cheer
• cringe      • cry
• cuddle      • dab
• dance       • die
• eat         • facepalm
• feed        • glomp
• happy       • hate
• holdhands   • hide
• highfive    • hug
• kill        • kiss
• laugh       • lick
• love        • lurk
• nervous     • no
• nom         • nuzzle
• panic       • pat
• peck        • poke
• pout        • run
• shoot       • shrug
• sip         • slap
• sleep       • snuggle
• stab        • tease
• think       • thumbsup
• tickle      • triggered
• twerk       • wag
• wave        • wink
• yes
</code>
<b>● Misceláneos:</b>

• /help - ¿Necesitas ayuda? Ven aquí.
• /feedback - ¿Tienes algún comentario? ¡Envíalo!
• /suggest - Envía una sugerencia para añadir tú gif.
• /donate - Apoya al programador del bot.