# Kisara-Bot

- Código fuente de [@kisara_bot](https://t.me/kisara_bot)

## ¿Que necesitas para alojar su propia versión en tú maquina?

- El bot kisara depende de un servidor api llamado [Ugoki](https://gitlab.com/ceda_ei/ugoki) para proveerle la base de datos de **gifs** de los comandos del rol play hacer una base de datos propia es opcional. (y opcionalmente puede incluirle la [Interfaz web de Ugoki](https://gitlab.com/ceda_ei/ugoki-frontend))

- Pasos para preparar y ejecutar el bot...

## Clonar el repositorio:

- `git clone https://gitlab.com/cristoferherame/kisara-bot.git`

## Instala las librerías y dependencias:

- `npm install`

## Instalar el paquete para procesar los gifs:

- `sudo apt install ffmpeg -y`

## Crea un bot de telegram con el [@botfather](https://t.me/botfather) y obtén el token:

- `export BOT_API_KEY="tú-token-del-bot"`

## Usa el comando **/info** del bot para ver el ID del chat de tu elección:

- `export FEEDBACK_ID="ID-del-chat-de-feedback"`

## Por penúltimo exportan el servidor **api** poniendo su enlace del servidor de [Ugoki](https://gitlab.com/ceda_ei/ugoki):

- `export UGOKI_ROOT="http://localhost:8000/"`
- Servidor api publico: **https://ugoki.webionite.com/api/**

## En la carpeta **raíz** del bot ejecutamos:

- `npm start`

## ¿Cómo facilitar el proceso de ejecución?

- En la carpeta raíz `cp example-start.sh start.sh`
- Configuramos el script `nano start.sh`
- Le damos permisos de ejecución `sudo chmod +x start.sh`
- Ejecutamos el script `./start.sh`

## Notas:

- Todos estos pasos fueron ejecutada en un servidor **Linux**, se recomienda utilizar **Ubuntu** o **Debian** para mayor comodidad.
- Enlace por defecto para acceder al servidor api desde el navegador: **http:/localhost:8000/docs**, recuerda crear las categorías de gifs tú mismo si haces tu propio servidor api de base de datos para almacenar gifs.

# Guía de uso de comandos

## Uso de comandos extras:

- Lanza una moneda virtual `/coin`
- Buscar contenido en la wiki `/wiki <contenido>`
- Buscar contenido en la wiki de ArchLinux `/arch_wiki <contenido>`
- Para matar a alguien responde con `/kys` o envía `/kys <@usuario>`
- Para insultar a alguien responde con `/insult` o envía `/insult <@usuario>`

## Comandos de juego de palabras:

- Obtén una pregunta al azar `/question`
- Obtén una palabra al azar `/word`
- Obten varias palabras al azar `/words <números>`
- Weebifica tú texto respondiendo o enviando `/weebify <texto>`
- Absurdifica tus textos respondiendo o enviando `/absurdify <texto>`
- Expande una oración respondiendo o enviando `/expand <texto>`

## Comandos de reacción uso general:

- Uso básico de reacción `/happy`
- Razón de reacción `/happy <razón>`
- Reacciones duales `/happy <@usuario>`
- Razón de reacción dual `/happy <@usuario> <razón>`
- Reacciones grupales `/happy <@usuario> <@usuario> <@etc>`
- Razón de reacción grupal `/happy <@usuario> <@etc> <razón>`

## Notas:

- Todos los comandos de reacción funcionan por igual y se usa `<@etc>` para indicar que se pueden añadir muchas más menciones.
- Se define a `<razón>` como el motivo propio o de los demás por su reacción.
- Las reacciones funcionan también `respondiendo` a los mensajes de los miembros del grupo.
